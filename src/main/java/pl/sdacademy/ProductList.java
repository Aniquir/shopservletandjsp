package pl.sdacademy;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by RENT on 2017-10-05.
 */
public class ProductList {

    List<Product> productList;

    public ProductList() {
        productList = new ArrayList<>();
        productList.add(new Product(1, "Słuchawki", "Grają", new BigDecimal(500)));
        productList.add(new Product(2, "Koszulka", "Jest czarna", new BigDecimal(100)));
        productList.add(new Product(3, "Laptop", "Jest najlepszy", new BigDecimal(8000)));
        productList.add(new Product(4, "Guziczek", "Na zapas", new BigDecimal(1)));
    }

    public List<Product> getAllProducts() {
        return productList;
    }

    public Optional<Product> findById(int id) {
        for (Product product : productList){
            if (product.getId() == id){
                return Optional.of(product);
            }
        }
        return Optional.empty();
    }
}

