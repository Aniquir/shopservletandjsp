package pl.sdacademy;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by RENT on 2017-10-06.
 */
@Data
@AllArgsConstructor
public class ShoppingItem {

    private int id;
    private int quantity;

}
