package pl.sdacademy;

import lombok.Getter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by RENT on 2017-10-06.
 */
public class ShoppingCart {

    @Getter
    private List<ShoppingItem> shoppingItems;

    public ShoppingCart(){
        shoppingItems = new ArrayList<>();
    }

    public void add(ShoppingItem shoppingItem){
        for (ShoppingItem existingItems : shoppingItems){
            if (existingItems.getId() == shoppingItem.getId()){
                int currentQuantity = existingItems.getQuantity();
                existingItems.setQuantity(currentQuantity + shoppingItem.getQuantity());
                return;
            }
        }
        shoppingItems.add(shoppingItem);
    }
}
