package pl.sdacademy;

import src.main.java.pl.sdacademy.HtmlGenerator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;

import static pl.sdacademy.ShopServlet.*;

/**
 * Created by RENT on 2017-10-05.
 */
@WebServlet(urlPatterns = {SHOW_ALL_PRODUCTS, SHOW_CART, PLACE_ORDER, SHOW_PRODUCT_DETAILS, ADD_TO_CART})
public class ShopServlet extends HttpServlet {
    static final String SHOW_ALL_PRODUCTS = "/showAllProducts";
    static final String SHOW_PRODUCT_DETAILS = "/showProductDetails";
    static final String SHOW_CART = "/showCart";
    static final String PLACE_ORDER = "/placeOrder";
    static final String ADD_TO_CART = "/addToCart";
    public static final String SHOPPING_CART = "shoppingCart";
    private ProductList productList = new ProductList();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestUri = req.getRequestURI();
        if (requestUri.endsWith(SHOW_ALL_PRODUCTS)) {
            showAllProducts(req, resp);
        } else if (requestUri.endsWith(SHOW_PRODUCT_DETAILS)){
            showProductDetails(req, resp);
        } else if (requestUri.endsWith(SHOW_CART)){
            showCart(req, resp);
        } else if (requestUri.endsWith(PLACE_ORDER)){
            placeOrder(req, resp);
        }
    }

    private void placeOrder(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = initializeWriter(resp);
        HttpSession session = req.getSession();
        session.removeAttribute(SHOPPING_CART);
        out.println("<h2>Zamówienie przekazane do realizacji. Koszyk został opróżniony</h2>");
        HtmlGenerator.generateFooter(out);
    }

    private void showCart(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = initializeWriter(resp);
        HttpSession session = req.getSession();
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute(SHOPPING_CART);
        if (shoppingCart != null && !shoppingCart.getShoppingItems().isEmpty()){
            printCart(shoppingCart, out);

        } else {
            out.println("<h2>Koszyk jest pusty</h2>");
        }
        HtmlGenerator.generateFooter(out);
    }

    private void printCart(ShoppingCart shoppingCart, PrintWriter out) {
        out.println("<h2>Zawartość koszyka</h2>");
        out.println("<ol>");

            for (ShoppingItem shoppingItem : shoppingCart.getShoppingItems()){

                Optional<Product> optional = productList.findById(shoppingItem.getId());
                if (optional.isPresent()){
                    Product product = optional.get();
                    out.println("<li>Produkt: " + product.getName()
                            + ", cena: " + product.getPrice()
                            + ", sztuk: " + shoppingItem.getQuantity()
                            + "</li>");
                }
            }

        out.println("</ol>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String requestUri = req.getRequestURI();
        if ( requestUri.endsWith(ADD_TO_CART)){
            addToCart(req, resp);
        }
}

    private void addToCart(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        int id = Integer.parseInt(req.getParameter("id"));
        int quantity = Integer.parseInt(req.getParameter("quantity"));
        ShoppingItem shoppingItem = new ShoppingItem(id, quantity);
        HttpSession session = req.getSession();
        ShoppingCart shoppingCart = (ShoppingCart) session.getAttribute(SHOPPING_CART);
        if (shoppingCart == null){
            shoppingCart = new ShoppingCart();
            session.setAttribute(SHOPPING_CART, shoppingCart);
        }
        shoppingCart.add(shoppingItem);
        PrintWriter out = initializeWriter(resp);
        out.println("<h2>udało się dodac produkt do koszyka</h2>");
        HtmlGenerator.generateFooter(out);
    }

    private PrintWriter initializeWriter(HttpServletResponse response) throws IOException {
        List<String> metaTags = new LinkedList<>();
        metaTags.add("charset=\"UTF-8\"");
        HtmlGenerator.generateHeader(response, "Sklep internetowy", metaTags);
        return response.getWriter();
    }

    private void showProductDetails(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = initializeWriter(resp);
        String parameterId = req.getParameter("id");
        int id = Integer.parseInt(parameterId);
        //optional wszedl w javie8
        Optional<Product> optional = productList.findById(id);
        if (optional.isPresent()) {
            printProductDetails(out, optional.get());
        }
            HtmlGenerator.generateFooter(out);
    }

    private void printProductDetails(PrintWriter out, Product product) {
        out.println("<h2>Informacje o produkcie</h2><br>");
        out.println("Nazwa produktu: " + product.getName() + "<br>");
        out.println("Opis produktu: " + product.getDescription() + "<br>");
        out.println("Cena produktu: " + product.getPrice() + "<br>");
        out.println("<form action=\"addToCart\" method=\"post\">");
        out.println("Liczba sztuk");
        out.println("<input type=\"text\" name=\"quantity\" value=\"1\">");
        out.println("<input type=\"hidden\" name=\"id\" value=\"" + product.getId() + "\">");
        out.println("<input type=\"submit\" value=\"Dodaj do koszyka\">");
        out.println("</form>");
    }

    private void showAllProducts(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        PrintWriter out = initializeWriter(resp);
        out.println("<ul>");
        for (Product product : productList.getAllProducts()) {
            out.println("<li>" + createProductDescription(product) + "</li>");
        }
        out.println("</ul>");
        HtmlGenerator.generateFooter(out);
    }

    private String createProductDescription(Product product) {
        return product.getName()
                + ", cena: " +product.getPrice()
                + createLink(product);
    }

    private String createLink(Product product) {
        return "<a href=\"showProductDetails?id=" + product.getId() + "\"> Pokaż szczegóły</a>";
    }
}




